(in-package :cl-naive-tailwind-kit)

(define-config (:tw-radio)
  (:config
   (:radio-box
    :class '("flex"
             "items-center"
             "w-full"
             "leading-normal"))
   (:radio-check
    :class '("flex"
             "items-center"
             "p-2"
             "leading-normal"
             "h-full")
    :input-class '("align-middle"
                   "leading-normal"
                   "h-em"
                   "w-em"))
   (:radio-label
    :class '("flex-1"
             "w-full"
             "leading-none"
             "bg-transparent"))))

(deftag (:radio-box)
  (:div :class (merge-classes
                (typecase (att :class)
                  (string (list (concatenate 'string (att :class) " ")))
                  (list (mapcar (lambda (class)
                                  (replace-all class "focus:" "focus-within:"))
                                (att :class))))
                (tag-setting :class :tw-radio))

        (new-children)))

(deftag (:radio-check)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw-radio))

         (:input :type "radio"
                 :class (merge-classes
                         (att :input-class)
                         (tag-setting :input-class :tw-radio))
                 :atts% (atts))))

(deftag (:radio-label)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw-radio))

         :tabindex (or (att :tabindex) "0")

         (new-children)))

(deftag (:tw-radio)
  (:radio-box :class (att :class)
              (when (or (class-exist "radio-left" (att :classes))
                        (not (class-exist "radio-right" (att :classes))))
                (:radio-check :class (att :check-class)
                              :input-class (att :check-input-class)))
              (:radio-label :class (att :label-class)
                            (new-children))
              (when (class-exist "radio-right" (att :classes))
                (:radio-check :class (att :check-class)
                              :input-class (att :check-input-class)))))

(with-sexp
  (expand
   (with-dom
     (:tw-radio :class '("rounded")
                "Oh Fuck!"))))
