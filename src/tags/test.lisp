(in-package :cl-naive-tailwind-kit)

(deftag (:test)

  "<div class=\"relative inline-block w-full text-zinc-700\">
  <select class=\"w-full pl-3 pr-6 text-3xl placeholder-zinc-600 border rounded-lg appearance-none focus:shadow-outline text-lg \" placeholder=\"Regular input\">
    <option>A regular sized select input</option>
    <option>Another option</option>
    <option>And one more</option>
  </select>
  <div class=\"absolute inset-y-0 right-0 text-3xl flex items-center px-2 pointer-events-none\">
    <svg class=\" h-full text-3xl fill-current\" viewBox=\"0 0 20 20\"><path d=\"M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"></path></svg>
  </div>
</div>
<br/>
<div class=\"relative inline-block w-full text-zinc-700\">
  <select class=\"w-full pl-3 pr-6 text-sm placeholder-zinc-600 border rounded-lg appearance-none focus:shadow-outline text-lg \" placeholder=\"Regular input\">
    <option>A regular sized select input</option>
    <option>Another option</option>
    <option>And one more</option>
  </select>
  <div class=\"absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none\">
    <svg class=\"w-4 h-4 fill-current\" viewBox=\"0 0 20 20\"><path d=\"M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"></path></svg>
  </div>
</div>
<br/>
<div class=\"inline-flex w-full h-full content-center bg-red-300 text-3xl\">
    <input type=\"checkbox\" id=\"checkbox-large-example\" class=\"self-center text-zinc-700 border rounded mr-2 min-h-5 min-w-5 \">
    <label :class \"flex-1\"  for=\"checkbox-large-example\">Check this custom Large checkbox</label>
</div>
<br/>
<div class=\"inline-flex w-full h-full mb-3 content-center focus-within:ring-2 text-2xl bg-red-300 rounded\">
  <span class=\"text-slate-300 bg-emerald-300 self-center p-3\">
    <i class=\"fas fa-lock\"></i>
  </span>
  <input type=\"text\" placeholder=\"Placeholder\"
   class=\"bg-transparent px-2 py-2 placeholder-slate-300 text-slate-600 border-0 outline-none focus:outline-none w-full flex-1 \"/>
</div>
<br/>
<div class=\"inline-flex w-full h-full mb-3 content-center focus-within:ring-2 text-2xl rounded\">

  <input type=\"text\" placeholder=\"Placeholder\"
   class=\"px-2 py-2 placeholder-slate-300 text-slate-600 border-0 outline-none focus:outline-none w-full flex-1 \"/>
  <span class=\"text-slate-300 bg-emerald-300 self-center p-3\">
    <i class=\"fas fa-lock\"></i>
  </span>
</div>
<br/>
<div>
  <div class=\"inline\">1</div>
  <div class=\"inline\">2</div>
  <div class=\"inline\">3</div>
</div>

<div style=\"width:300px;\">
  <div class=\"flex w-full items-center leading-normal border border-zinc-400 bg-zinc-700 text-zinc-400 rounded text-5xl\">
        <input class=\"flex-1 w-full px-2 bg-transparent text-zinc-400\"
        placeholder=\"text-5xl text with icon right. \">
        <span class=\"bg-blue-500 rounded-r\">
          <button class=\"bg-blue-500 rounded-r \">Ooops
          </button>
        </span>
  </div>
</div>

<div class=\"w-full max-w-lg overflow-hidden rounded-lg shadow-lg sm:flex\">
  <div class=\"w-full sm:w-1/3\">
    <img class=\"object-cover w-full h-48\" src=\"https://images.pexels.com/photos/853199/pexels-photo-853199.jpeg?auto=compress&cs=tinysrgb&h=650&w=940\" alt=\"Flower and sky\"/>
  </div>
  <div class=\"flex-1 px-6 py-4\">
    <h4 class=\"mb-3 text-xl font-semibold tracking-tight text-zinc-800\">This is the title</h4>
    <p class=\"leading-normal text-zinc-700\">Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.</p>
  </div>
</div>

")

