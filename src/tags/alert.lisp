(in-package :cl-naive-tailwind-kit)

(define-config (:tw-alert)
  (:config
   (:alert-box
    :class '("px-6"
             "py-4"
             ;; "border-0"
             "rounded"
             "relative"
             "mb-4"))
   (:alert-icon
    :class '("text-xl" "inline-block" "mr-5" "align-middle")
    :icon-class nil)
   (:alert-body
    :class '("inline-block" "align-middle" "mr-8"))
   (:alert-button
    :class '("absolute"
             "bg-transparent"
             "text-normal"
             "font-semibold"
             "leading-none"
             "right-0"
             "top-0"
             "mt-5"
             "mr-4"
             "outline-none"
             "focus:outline-none")
    :icon-class "fa fa-times"
    :onclick "this.parentNode.style.display='none';")))

(deftag (:alert-box (element new-children))
  (:div :class (merge-classes
                '("z-0")
                (att :class)
                (tag-setting :class :tw.tw-alert))
        new-children))

(deftag (:alert-icon)
  (:span :class (merge-classes (att :class)
                               (tag-setting :class :tw.tw-alert))
         (:i :class (att :icon))))

(deftag (:alert-body)
  (:span :class (merge-classes
                 '("z-20")
                 (att :class)
                 (tag-setting :class :tw.tw-alert))
         (new-children)
         (when (att :watermark)
           (:watermark :class (att :watermark)))))

(deftag (:alert-button)
  (:button :class (merge-classes (att :class)
                                 (tag-setting :class :tw.tw-alert))
           :onclick (or
                     (tag-setting :onclick :tw.tw-alert)
                     "this.parentNode.style.display='none';")

           (:i :class (or
                       (att :icon)
                       (tag-setting :icon-class :tw.tw-alert)))))

(deftag (:tw-alert)
  (:alert-box :class (att :class)
              :color (att :color)
              :corners (att :corners)

              ;;icon-clas overrides the body class completely.
              (when (att :icon)
                (:alert-icon :icon (att :icon)
                             :class (att :icon-class)))

              ;;Body-Class overrides the body class completely.
              (:alert-body :class (att :body-class)
                           :watermark (att :watermark)
                           (new-children))
              (:alert-button :class (att :button-class)
                             :icon (att :button-icon))))

#|
(with-sexp
(expand
(with-dom
(:tw-alert :class '("primary" "rounded")
"Oh Fuck!"))))

(with-sexp
(expand
(with-dom
(:tw-alert :class '("primary" "outline")
"Oh Fuck!"))))

(with-sexp
(expand
(with-dom
(:tw-alert :class '("secondary" "rounded")
"Oh Fuck!"))))
|#
