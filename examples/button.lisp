(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-button
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :class ,(lts "bg-blue-500"
                                            "text-blue-50"

                                            "focus:bg-blue-400"
                                            "focus:text-blue-50"

                                            "hover:bg-blue-400"
                                            "hover:text-blue-50"

                                            "dark:bg-zinc-700"
                                            "dark:text-zinc-400"

                                            "dark:focus:bg-zinc-600"
                                            "dark:focus:text-zinc-300"

                                            "dark:hover:bg-zinc-600"
                                            "dark:hover:text-zinc-300"))
                              (:name :outline
                               :class ,(lts "border"
                                            "border-blue-500"
                                            "text-blue-500"

                                            "focus:bg-blue-400"
                                            "focus:text-blue-50"

                                            "hover:bg-blue-400"
                                            "hover:text-blue-50"

                                            "dark:bg-zinc-700"
                                            "dark:border-zinc-600"
                                            "dark:text-zinc-400"

                                            "dark:focus:bg-zinc-600"
                                            "dark:focus:text-zinc-300"

                                            "dark:hover:bg-zinc-600"
                                            "dark:hover:text-zinc-300"))))
                     (:name "secondary"
                      :types ((:name :default
                               :class ,(lts "bg-zinc-500"
                                            "text-zinc-50"

                                            "focus:bg-zinc-700"
                                            "focus:text-zinc-400"

                                            "hover:bg-zinc-700"
                                            "hover:text-zinc-400"

                                            "dark:bg-zinc-800"
                                            "dark:text-zinc-500"

                                            "dark:focus:bg-zinc-700"
                                            "dark:focus:text-zinc-500"

                                            "dark:hover:bg-zinc-700"
                                            "dark:hover:text-zinc-500"))
                              (:name :outline
                               :class ,(lts "border"
                                            "border-zinc-500"

                                            "text-zinc-500"

                                            "focus:bg-zinc-600"
                                            "focus:text-zinc-400"

                                            "hover:bg-zinc-600"
                                            "hover:text-zinc-400"

                                            "dark:bg-zinc-800"

                                            "dark:border-zinc-700"
                                            "dark:text-zinc-500"

                                            "dark:focus:bg-zinc-700"
                                            "dark:focus:text-zinc-500"

                                            "dark:hover:bg-zinc-700"
                                            "dark:hover:text-zinc-500")))))
                    :oic-button-round-icon ,(lts
                                             "inline-flex" "items-center"
                                             "justify-center" "mr-2"
                                             "rounded-full")
                    :oic-button-pill ,(lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
                                           "rounded-full")
                    :oic-button ,(lts "font-bold" "px-4" "py-2" "mr-1" "mb-1")

                    :class ,(lts "outline-none"
                                 "focus:outline-none"
                                 "ease-linear"
                                 "transition-all"
                                 "duration-150"
                                 "shadow-none"
                                 "rounded"))))

(setf (gethash "/button" (handlers *site*))
      (lambda (script-name)
        (declare (ignore script-name))

        (with-html-to-string
          "<!doctype html>"
          (::page

           (:div :class "p-2"
                 (:h2 :class  "uppercase" "Button examples build with raw classes:")
                 (:hr)
                 (:br)
                 (let ((*theme* nil))
                   (cl-who:htm
                    (:tw-button
                     :class "bg-blue-500 active:bg-blue-400 rounded focus:outline-none"
                     "Button Ho!")

                    (:tw-button
                     :class  (lts "border" "border-blue-500" "text-blue-500"
                                  "active:bg-blue-500" "active:text-zinc-50" "rounded"
                                  "focus:outline-none")
                     "Button Ho!")

                    (:tw-button :class "pill bg-blue-500 active:bg-blue-400 focus:outline-none"
                                "Button Ho!")

                    (:tw-button :class (lts "pill border" "border-blue-500" "text-blue-500"
                                            "active:bg-blue-500" "active:text-zinc-50"
                                            "focus:outline-none")
                                "Button Ho!")

                    (:br)

                    (:tw-button :class "round-icon bg-blue-500 focus:outline-none"
                                :icon "fas fa-wind")

                    (:tw-button :class (lts "round-icon" "border" "border-blue-500"
                                            "text-blue-500" "focus:outline-none")
                                :icon "fas fa-wind")

                    (:tw-button :class (lts "round-icon" "hover:bg-blue-100"
                                            "text-zinc-500" "focus:outline-none")
                                :icon "fas fa-wind")
                    (:br)
                    (:tw-button :class (lts "round-icon" "border" "border-blue-500"
                                            "text-blue-500" "text-3xl"
                                            "focus:outline-none")
                                :size "20"
                                :icon "fas fa-wind"))))
           (:hr)
           (:br)

           (:div :class "p-3 dark:bg-zinc-900"

                 (:h2 :class "uppercase dark:text-zinc-400"
                      "Button examples based on theme:")

                 (:hr)
                 (:br)

                 (:tw-button :class "primary" "Button Ho!")

                 (:tw-button
                  :class  "primary outline"
                  "Button Ho!")

                 (:tw-button :class "primary pill"
                             "Button Ho!")

                 (:tw-button :class "primary pill outline"
                             "Button Ho!")

                 (:br)

                 (:tw-button :class "primary round-icon"
                             :icon "fas fa-wind")

                 (:tw-button :class "primary round-icon outline"
                             :icon "fas fa-wind")

                 (:tw-button :class "round-icon hover:bg-blue-100 text-zinc-500"
                             :icon "fas fa-wind")
                 (:br)
                 (:tw-button :class (lts "primary" "round-icon" "outline" "text-3xl")
                             :size "20"
                             :icon "fas fa-wind")
                 (:br)
                 (:br)
                 (:tw-button :class "secondary" "Button Ho!")

                 (:tw-button
                  :class  "secondary outline"
                  "Button Ho!")

                 (:tw-button :class "secondary pill"
                             "Button Ho!")

                 (:tw-button :class "secondary pill outline"
                             "Button Ho!")

                 (:br)

                 (:tw-button :class "secondary round-icon"
                             :icon "fas fa-wind")

                 (:tw-button :class "secondary round-icon outline"
                             :icon "fas fa-wind")

                 (:tw-button :class "round-icon hover:bg-blue-100 text-zinc-500"
                             :icon "fas fa-wind")
                 (:br)
                 (:tw-button :class (lts "secondary" "round-icon" "outline" "text-3xl")
                             :size "20"
                             :icon "fas fa-wind"))
           (:hr)
           (:br)
           (:h2 :class "uppercase "
                "Completely customized button:")

           (:hr)

           (:br)
           (:tw-button :oic-button (lts "font-bold"
                                        "px-4"
                                        "py-2"
                                        "rounded-lg"
                                        "shadow"
                                        "hover:shadow-md"
                                        "outline-none"
                                        "focus:outline-none"
                                        "mr-1"
                                        "mb-1"
                                        "ease-linear"
                                        "transition-all"
                                        "duration-150")
                       :class (lts "bg-red-500"
                                   "active:bg-red-400"
                                   "text-zinc-50"
                                   "uppercase"
                                   "text-5xl")
                       "rounded-lg!")

           (:br)))))
