(in-package :tailwind-kit.examples.clean)

(define-config (:tw.tw-alert)
  (:tw.config
   (:tw.color-scheme
    (:tw.primary
     (:tw.default
      '(
        "bg-blue-500"
        "text-blue-50"
        "theme-marker"
        "dark:bg-zinc-700"
        "dark:text-zinc-400"))
     (:tw.outline
      ()))
    (:tw.secondary
     (:tw.default
      ())
     (:tw.outline
      ())))
   (:tw.alert-box
    :class '("px-6"
             "py-4"
             "border-0"
             "rounded"
             "relative"
             "mb-4"))
   (:tw.alert-icon
    :class '("text-xl" "inline-block" "mr-5" "align-middle")
    :icon-class nil)
   (:tw.alert-body
    :class '("inline-block" "align-middle" "mr-8"))
   (:tw.alert-button
    :class '("absolute"
             "bg-transparent"
             "text-normal"
             "font-semibold"
             "leading-none"
             "right-0"
             "top-0"
             "mt-5"
             "mr-4"
             "outline-none"
             "focus:outline-none")
    :icon-class '("fa" "fa-times")
    :onclick "this.parentNode.style.display='none';")))

(defmacro with-current-package (&body body)
  `(let ((*package* (or (find-package #.(package-name *package*))
                        (error "Cannot find the package named ~S"
                               #.(package-name *package*)))))
     ,@body))

(setf (gethash "/alert" (handlers *site*))
      (lambda (script-name)
        (declare (ignore script-name))
        (let ((*package* (find-package :tailwind-kit.examples.clean)))
          (with-abt
            (:page
             (let ((cl-naive-ccf::*configs* (make-hash-table)))
               (:div :class "p-2"

                     (:h2 :class  "uppercase" "Alert examples build with raw classes:")
                     (:hr)
                     (:br)

                     (:tw.tw-alert :class '("bg-blue-500 capitalize")
                                   :icon '("fa" "fa-exclamation")
                                   "Alert with fa-exclamation icon.")

                     (:tw.tw-alert :id "danger-alert"
                                   :class '("bg-red-500 text-zinc-50")
                                   :button-icon '("fa-solid" "fa-toilet")
                                   "Alert with a different closing icon.")))
             (:hr)
             (:br)

             (:div :class "p-2 dark:bg-zinc-900 dark:text-zinc-200"

                   (:h2 :class "uppercase dark:text-zinc-400"
                        "Alert examples based on theme:")

                   (:hr)
                   (:br)

                   (:tw.tw-alert :class '("primary" "capitalize")
                                 :icon '("fa" "fa-exclamation")
                                 "Alert primary")

                   (:tw.tw-alert  :class '("primary" "capitalize" "outline")
                                  "Alert primary outline")

                   (:tw.tw-alert :class '("secondary" "capitalize")
                                 :icon '("fa" "fa-exclamation")
                                 "Alert secondary")

                   (:tw.tw-alert  :class '("secondary" "capitalize" "outline")
                                  "Alert secondary outline"))

             (:div :class "p-2"
                   (:hr)
                   (:br)
                   (:h2 :class "uppercase "
                        "Alerts can have html as content:")

                   (:hr)
                   (:tw.tw-alert :class '("bg-yellow-500" "text-zinc-50")
                                 :icon '("fa-solid" "fa-toilet")
                                 :icon-class '("text-9xl" "inline-block" "mr-5" "align-middle")

                                 (:div :class "font-bold"
                                       (:p
                                        "Alert custom Ho Ho!")
                                       (:p
                                        "Alert custom Ho Ho!")
                                       (:p
                                        "Alert custom Ho Ho!")
                                       (:p
                                        "Alert custom Ho Ho!")
                                       (:p
                                        "Alert custom Ho Ho!")
                                       (:p
                                        "Alert custom Ho Ho!")))))))))
