const colors = require('tailwindcss/colors')

module.exports = {
    content: [
        // Example content paths...
        './**/*.{js,lisp}',
    ],

    darkMode: 'class', // false or 'media' or 'class'
    theme: {
        extend: {
            spacing: {
                'em': '1em',
            }
        }
    },
    plugins: [

    ],
}

