(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-checkbox
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-checkbox-label ,(lts "text-blue-500"
                                                         "dark:text-zinc-400")

                               :oic-outer ,(lts "text-blue-500"
                                                "focus-within:ring-blue-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-checkbox-label ,(lts "text-blue-500"
                                                         "dark:text-zinc-400")
                               :oic-outer ,(lts "text-blue-500"
                                                "border"
                                                "border-blue-400"
                                                "focus-within:ring-blue-400"
                                                "dark:border-zinc-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-checkbox-label ,(lts
                                                     "text-zinc-500"

                                                     "dark:text-zinc-400")

                               :oic-outer ,(lts "text-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-checkbox-label ,(lts
                                                     "text-zinc-500"
                                                     "dark:text-zinc-400")
                               :oic-outer ,(lts "text-zinc-500"
                                                "border"
                                                "border-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500")))))

                    :oic-outer,(lts "flex"
                                    "items-center"
                                    "w-full"
                                    "leading-normal")

                    :oic-checkbox ,(lts "flex"
                                        "items-center"
                                        "p-2"
                                        "leading-normal"
                                        "h-full")
                    :oic-checkbox-label ,(lts "flex-1"
                                              "w-full"
                                              "leading-none"
                                              "bg-transparent")

                    :class ,(lts "rounded"))))

(setf (gethash "/checkbox" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil))
               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Checkbox examples build with raw classes:")
                      (:hr)
                      (:br)
                      (:tw-checkbox "Vanilla Text Checkbox.")
                      (:br)
                      (:div :class "grid grid-flow-row grid-cols-2 gap-2"

                            (:tw-checkbox :class "checkbox-left"
                                          "Text checkbox with checkbox to left.")
                            (:tw-checkbox :class "checkbox-right"
                                          "Text checkbox with checkbox to right.")
                            (:tw-checkbox :class "checkbox-left text-sm"
                                          "text-sm text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-sm"
                                          "text-sm text with checkbox right.")
                            (:tw-checkbox :class "checkbox-left text-lg"
                                          "text-lg text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-lg"
                                          "text-lg text with checkbox right.")
                            (:tw-checkbox :class "checkbox-left text-xl"
                                          "text-xl text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-xl"
                                          "text-xl text with checkbox right.")
                            (:tw-checkbox :class "checkbox-left text-2xl"
                                          "text-2xl text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-2xl"
                                          "text-2xl text with checkbox right.")
                            (:tw-checkbox :class "checkbox-left text-3xl"
                                          "text-3xl text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-3xl"
                                          "text-3xl text with checkbox right. ")
                            (:tw-checkbox :class "checkbox-left text-4xl"
                                          "text-4xl text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-4xl"
                                          "text-4xl text with checkbox right. ")
                            (:tw-checkbox :class "checkbox-left text-5xl"
                                          "text-5xl text with checkbox left.")
                            (:tw-checkbox :class "checkbox-right text-5xl"
                                          "text-5xl text with checkbox right. ")))))
             (:hr)
             (:br)
             (:div :class "p-3 dark:bg-zinc-900"
                   (:h2 :class "uppercase dark:text-zinc-400"
                        "Checkbox examples based on theme:")
                   (:hr)
                   (:br)
                   (:tw-checkbox :class "primary"
                                 "Vanilla Text Checkbox.")
                   (:br)
                   (:tw-checkbox :class "primary outline"
                                 "Vanilla Text Checkbox.")
                   (:br)
                   (:tw-checkbox :class "primary checkbox-left text-xl"
                                 "text-xl text with checkbox left.")
                   (:br)
                   (:tw-checkbox :class "primary outline checkbox-right text-xl"
                                 "text-xl text with checkbox right. ")
                   (:br)
                   (:tw-checkbox :class "secondary"
                                 "Vanilla Text Checkbox.")
                   (:br)
                   (:tw-checkbox :class "secondary outline"
                                 "Vanilla Text Checkbox.")
                   (:br)
                   (:tw-checkbox :class "secondary checkbox-left text-xl"
                                 "text-base xl with checkbox left.")
                   (:br)
                   (:tw-checkbox :class "secondary outline checkbox-right text-xl"
                                 "text-xl text with checkbox right. "))

             (:br)))))
