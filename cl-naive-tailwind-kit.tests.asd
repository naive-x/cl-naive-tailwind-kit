(defsystem "cl-naive-tailwind-kit.tests"
  :description "Tests for cl-naive-tailwind-kit"
  :version "2022.9.16"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tailwind-kit :cl-naive-tests :cl-naive-dom :cl-naive-ccf)
  :components ((:file "tests/package")
               (:file "tests/tests" :depends-on ("tests/package"))
               (:file "tests/test-alert" :depends-on ("tests/tests"))
               (:file "tests/test-button" :depends-on ("tests/tests"))))

